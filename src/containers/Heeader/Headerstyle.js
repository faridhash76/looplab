import React, {Component} from "react";
import {
    Container,
    Row,
    Col,
    Card,
    CardBody,
    Form,
    FormGroup,
    Input,
    Button
}from "reactstrap";
class Headerstyle extends Component{
    render() {
        return(
            <header id="home-section">
                <div className="dark-overlay">
                    <Container className='home-inner '>
                        <Row>
                            <Col lg="8" className="d-none d-lg-block">
                                <h1 className="display-4">
                                    build
                                    <strong>social profiles</strong>
                                    and gain revenue
                                    <strong>profits</strong>
                                </h1>
                                <div className='d-flex'>
                                    <div className="p-4 align-self-start">
                                        <i className="fas fa-check fa-2x"/>
                                    </div>
                                    <div className='p-4 align-self-end'>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores corporis doloribus nam odio ratione soluta?
                                    </div>
                                </div>
                                <div className='d-flex'>
                                    <div className="p-4 align-self-start">
                                        <i className="fas fa-check fa-2x"/>
                                    </div>
                                    <div className='p-4 align-self-end'>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores corporis doloribus nam odio ratione soluta?
                                    </div>
                                </div>
                                <div className='d-flex'>
                                    <div className="p-4 align-self-start">
                                        <i className="fas fa-check fa-2x"/>
                                    </div>
                                    <div className='p-4 align-self-end'>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores corporis doloribus nam odio ratione soluta?
                                    </div>
                                </div>
                            </Col>
                            <Col lg="4">
                                <Card className="bg-primary text-center card-form">
                                    <CardBody>
                                        <h3>Sign up today</h3>
                                        <p>Please fill out form to register</p>
                                        <Form>
                                            <FormGroup>
                                                <Input type="text" name="username" id="username" size="lg" placeholder="Username"/>
                                            </FormGroup>
                                            <FormGroup>
                                                <Input type="email" name="email" id="email" size="lg" placeholder="Email"/>
                                            </FormGroup>
                                            <FormGroup>
                                                <Input type="password" name="password" id="password" size="lg" placeholder="Password"/>
                                            </FormGroup>
                                            <FormGroup>
                                                <Input type="password" name="confirm-password" id="confirm-password" size="lg" placeholder="ConfirmPassword"/>
                                            </FormGroup>
                                            <Input type="submit" value='Submit' className='btn btn-outline-light btn-block' />
                                        </Form>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </header>
        )
    }
}
export default Headerstyle;