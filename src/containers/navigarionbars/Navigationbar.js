import React, {Component} from "react";
import{
    Navbar,
    Container,
    NavbarBrand,
    Collapse,
    Nav,
    NavItem,
    NavLink


}from "reactstrap";
import '../../assets/styles/header.css'
import NavbarToggler from "reactstrap/lib/NavbarToggler";


class Navigationbar extends Component {
    constructor(props) {
        super(props);
        this.state={
            isOpen:false
        }
    }



    toggle=()=>{
        this.setState(prev=>{
            return{
                isOpen:!prev.isOpen
            }
        })

    }
    render() {
        const {isOpen}=this.state;

        return(
            <Navbar color="dark" dark expand="sm" fixed="top">
                <Container>
                    <NavbarBrand href="/">LoopLAB</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink href="#home">Home</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#Explore">Explore</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#Create">Create</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#Share">Share</NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Container>

            </Navbar>

        )
    }
}
export default Navigationbar;